<?php
/**
 * Plugin Dépublication
 *
 * Utilisation de pipelines SPIP
 *
 * @author Matthieu Marcillaud
 * @license GNU/GPL
 * @copyright 2010-2014
 * @package SPIP\Plugin\Depublication
 */

if (!defined('DEPUBLICATION_PERIODICITE_CRON')) {
	/**
	 * Périodicité de la tâche cron de dépublication
	 * @var int Tous les jours par défaut */
	define('DEPUBLICATION_PERIODICITE_CRON', 1*24*3600);
}

if (!defined('DEPUBLICATION_STATUT_PAR_DEFAUT')) {
	/**
	 * Statut proposé par défaut pour la dépublication
	 * @var string Code du statut */
	define('DEPUBLICATION_STATUT_PAR_DEFAUT', 'depublie');
}


/**
 * Ajouter au cron de la fonction de dépublication d'articles
 *
 * @param array $taches Liste des taches à effectuer
 * @return array Liste des taches à effectuer
**/
function depublication_taches_generales_cron($taches){
	$taches['depublier_objets'] = DEPUBLICATION_PERIODICITE_CRON;
	return $taches;
}


/**
 * Retourne la liste des objets sur lesquels on doit
 * ajouter une gestion de date de dépublication
 *
 * @return array Liste de type d'objet
**/
function depublication_get_objets_valides() {
	include_spip('inc/config');
	$objets_depuliables = lire_config('depublication/depublie_objets');
	return $objets_depuliables;
}

/**
 * Retourne la liste des statuts possibles après dépublication
 *
 * À peu près tout par défaut sauf 'publie'
 *
 * @param string $objet Type d'objet
 * @param array $desc Description de l'objet éditorial
 * @return array Couple code => texte de statut
**/
function depublication_get_statuts_valides($objet, $desc = array()) {
	static $statuts = array();
	if (isset($statuts[$objet])) {
		return $statuts[$objet];
	}

	if (!is_array($desc) OR !$desc) {
		$table = table_objet($objet);
		$trouver_table = charger_fonction('trouver_table','base');
		$desc = $trouver_table($table);
	}

	if (!is_array($desc) OR !$desc) {
		return $statuts[$objet] = array();
	}

	include_spip('prive/formulaires/instituer_objet');
	$liste = lister_statuts_proposes($desc);
	// on ne dépublie pas un article publié !
	unset($liste['publie']);

	return $statuts[$objet] = $liste;
}


/**
 * Ajouter la gestion de notre date au formulaire dater (chargement)
 *
 * @param array $flux
 * @return array $flux
**/
function depublication_formulaire_charger($flux){
	if ($flux['args']['form'] != 'dater') {
		return $flux;
	}

	$objet    = $flux['data']['objet'];
	$id_objet = $flux['data']['id_objet'];

	if (!in_array($objet, depublication_get_objets_valides())) {
		return $flux;
	}

	$_id_objet = id_table_objet($objet);
	$table = table_objet($objet);
	$trouver_table = charger_fonction('trouver_table','base');
	$desc = $trouver_table($table);

	if (!$desc) {
		return false;
	}

	if (!isset($desc['field']['date_depublication'])) {
		return false;
	}

	if (!isset($desc['field']['statut_depublication'])) {
		return false;
	}

	$row = sql_fetsel('date_depublication, statut_depublication, statut', $desc['table'], "$_id_objet=".intval($id_objet));

	$annee = $mois = $jour = $heure = $minute = 0;
	if ($regs = recup_date($row['date_depublication'], false)) {
		$annee  = $regs[0];
		$mois   = $regs[1];
		$jour   = $regs[2];
		$heure  = $regs[3];
		$minute = $regs[4];
	}

	// données pour la date
	$flux['data']['_label_date_depublication']   = _T('depublication:label_date_depublication');
	$flux['data']['aucune_date_depublication'] = intval($annee) ? false : true;
	$flux['data']['afficher_date_depublication'] = $row['date_depublication'];

	$date_texte  = dater_formater_saisie_jour($jour,$mois,$annee);
	$heure_texte = "$heure:$minute";
	$flux['data']['date_depublication_jour']  = ($date_texte == '0000') ? '' : $date_texte;
	$flux['data']['date_depublication_heure'] = ($heure_texte == '00:00') ? '' : $heure_texte;

	// données pour le statut
	$flux['data']['_label_statut_depublication'] = _T('depublication:label_statut_depublication');
	$flux['data']['_statuts'] = depublication_get_statuts_valides($objet, $desc);
	$flux['data']['a_ete_depublie'] = $row['statut_depublication'] == 'done';
	if ((!$statut_depublication = $row['statut_depublication']) OR ($statut_depublication == 'done')) {
		$statut_depublication = DEPUBLICATION_STATUT_PAR_DEFAUT;
	}
	$flux['data']['statut_depublication'] = $statut_depublication;

	return $flux;
}


/**
 * Ajouter la gestion de notre date au formulaire dater (verifications)
 *
 * @param array $flux
 * @return array $flux
**/
function depublication_formulaire_verifier($flux) {
	if ($flux['args']['form'] != 'dater'){
		return $flux;
	}

	$objet    = $flux['args']['args'][0];
	$id_objet = $flux['args']['args'][1];

	if (!in_array($objet, depublication_get_objets_valides())) {
		return $flux;
	}

	$k = 'date_depublication';

	if ($v = _request($k . "_jour") AND !dater_recuperer_date_saisie($v)) {
		$flux['data'][$k] = _T('format_date_incorrecte');
	}
	elseif ($v=_request($k."_heure") AND !dater_recuperer_heure_saisie($v)) {
		$flux['data'][$k] = _T('format_heure_incorrecte');
	}

	if ($v = _request('statut_depublication') AND !in_array($v, array_keys(depublication_get_statuts_valides($objet)))) {
		$flux['data']['statut_depublication'] = _T('depublication:erreur_statut_incorrect');
	}

	return $flux;
}


/**
 * Ajouter la gestion de notre date au formulaire dater (traitements)
 *
 * @param array $flux
 * @return array $flux
**/
function depublication_formulaire_traiter($flux) {
	if ($flux['args']['form'] != 'dater'){
		return $flux;
	}

	$objet    = $flux['args']['args'][0];
	$id_objet = $flux['args']['args'][1];

	if (!in_array($objet, depublication_get_objets_valides())) {
		return $flux;
	}

	if (_request('changer')){
		$_id_objet = id_table_objet($objet);
		$table = table_objet($objet);
		$trouver_table = charger_fonction('trouver_table','base');
		$desc = $trouver_table($table);

		if (!$desc)
			return array('message_erreur'=>_L('erreur')); #impossible en principe

		$set = array();

		if (!_request('date_depublication_jour')) {
			$set['date_depublication'] = sql_format_date(0,0,0,0,0,0);
			$set['statut_depublication'] = '';
		}
		else {
			if (!$d = dater_recuperer_date_saisie(_request('date_depublication_jour'))) {
				$d = array(date('Y'),date('m'),date('d'));
			}
			if (!$h = dater_recuperer_heure_saisie(_request('date_depublication_heure'))) {
				$h = array(0,0);
			}
			$set['date_depublication'] = sql_format_date($d[0], $d[1], $d[2], $h[0], $h[1]);
			$set['statut_depublication'] = _request('statut_depublication');
		}

		include_spip('action/editer_objet');
		objet_modifier($objet, $id_objet, $set);
	}

	set_request('date_depublication_jour');
	set_request('date_depublication_heure');

	return $flux;
}


/**
 * Ajouter la gestion de notre date au formulaire dater (vue)
 *
 * @param array $flux
 * @return array $flux
**/
function depublication_formulaire_fond($flux){
	if ($flux['args']['form'] != 'dater') {
		return $flux;
	}

	$env = $flux['args']['contexte'];

	if (!in_array($env['objet'], depublication_get_objets_valides())) {
		return $flux;
	}

	if (!$id_objet = $env['id_objet']) {
		return $flux;
	}

	// insertion des saisies HTML
	if (($p = strpos($flux['data'],"<!--extra-->"))!==false){
		$input = recuperer_fond('prive/formulaires/inc-dater-depublication', $env);
		$flux['data'] = substr_replace($flux['data'],$input,$p,0);
	}
	// insertion du JS en plus sur le bouton [changer]
	if (($p = strpos($flux['data'],"f.find('.boutons')"))!==false){
		$js = "f.find('span.input').show('fast');";
		$js .= "f.find('.editer_statut_depublication').show('fast');";
		$flux['data'] = substr_replace($flux['data'],$js,$p,0);
	}

	return $flux;
}

/**
 * Ajouter les articles en statut "dépublié" sur les vues de rubriques
 *
 * @pipeline affiche_enfants
 *
 * @param array $flux Données du pipeline
 * @return array       Données du pipeline
 **/
function depublication_affiche_enfants($flux) {
	// TODO Si pas d'articles dépubliés dans la rubrique le bloc d'affiche quand même. A voir comment ne pas l'afficher
	if (isset($flux['args']['exec'])
		and $e = trouver_objet_exec($flux['args']['exec'])
		and $e['type'] == 'rubrique'
		and $e['edition'] == false
	) {
		$id_rubrique = $flux['args']['id_rubrique'];
			$flux['data'] .= recuperer_fond("prive/objets/liste/articles",
				array(
						'statut' => 'depublie',
						'id_rubrique' => $id_rubrique,
						'nb' => '30',
						'env' => ' ',
						'ajax' => ' ',
						'titre'=> _T('depublication:pages_depubliees'),
						'sinon' => _T('depublication:aucune_page')
						)
			);
	}

	return $flux;
}
