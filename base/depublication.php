<?php
/**
 * Plugin Dépublication
 * 
 * @author Matthieu Marcillaud
 * @license GNU/GPL
 * @copyright 2010-2014
 * @package SPIP\Plugin\Depublication
 */

/**
 * Déclaration des champs SQL
 * 
 * Déclarer les champs additionnels `date_depublication` et
 * `statut_depublication` sur la table des articles.
 * 
 * @param array $tables
 *     Définition de tous les objets éditoriaux
 * @return array $tables
 *     Définition (complétée) de tous les objets éditoriaux
 */
function depublication_declarer_tables_objets_sql($tables)
{

	include_spip('inc/config');
	$tables_date_depublication = lire_config('depublication/depublie_objets');
	
	foreach($tables_date_depublication as $table){
		if($table != ''){
			$tables[$table]['field'] += array(
				'date_depublication'   => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
				'statut_depublication' => "varchar(10) DEFAULT '0' NOT NULL",
			);
		
			// statut dépublication après 'publie'
			$statuts = array(
				'statut_titres' => 'depublication:statut_objet_depublie_titre',
				'statut_textes_instituer' => 'depublication:statut_objet_depublie_texte'
			);
		
			foreach ($statuts as $cle => $i18n) {
				$liste = $tables[$table][$cle];
				if ($key = array_search('publie', array_keys($liste))) {
					$liste = array_slice($liste, 0, ++$key, TRUE)
							+ array('depublie' => $i18n)
							+ array_slice($liste, $key, NULL, TRUE);
				}
				$tables[$table][$cle] = $liste;
			}
		
			// l'image du statut
			if (!isset($tables[$table]['statut_images'])) {
				// obligé de coller les anciens (statut_images non défini sur spip_articles
				$tables[$table]['statut_images'] = array(
						'prepa'    => 'puce-preparer-8.png',
						'prop'     => 'puce-proposer-8.png',
						'publie'   => 'puce-publier-8.png',
						'refuse'   => 'puce-refuser-8.png',
						'poubelle' => 'puce-supprimer-8.png',
						'depublie' => 'puce-depublie-8.png',
				);
			} else {
				$tables[$table]['statut_images']['depublication'] = 'puce-depublie-8.png';
			}
		
			// afficher comme élément publie ?
			if (lire_config('depublication/visibilite_publie')) {
				$tables[$table]['statut'][0]['publie'] .= ',depublie';
			}
		
			// afficher dans les boucles en previsu ?
			if (lire_config('depublication/visibilite_previsu')) {
				$tables[$table]['statut'][0]['previsu'] .= ',depublie';
			}
		}
	}

	return $tables;
}


