<?php
/**
 * Plugin Dépublication
 *
 * Filtres et critères de boucles
 *
 * @author Matthieu Marcillaud
 * @license GNU/GPL
 * @copyright 2010-2016
 * @package SPIP\Plugin\Depublication
 */


// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;


/**
 * Compilation du critère `{depublie}`
 * 
 * Ce critère va particulièrement de paire avec l'option de configuration 
 * affichant les éléments dépubliés dans les boucles par défaut.
 * 
 * Il permet alors de sélectionner ou d'enlever des boucles
 * uniquement les éléments dépubliés.
 * 
 * - `<BOUCLE_art(ARTICLES){depublies}> ... `
 *   sélectionnera les articles visibles ayant le statut 'depublie'
 * 
 * - `<BOUCLE_art(ARTICLES){!depublies} ...`
 *   sélectionnera les articles visibles non dépubliés
 * 
 * À la différence d'utiliser directement le critère `{statut}` 
 * pour sélectionner les statuts désirés, ce critère ne surpasse pas 
 * le comportement de SPIP par défaut lorsqu'on utilise `{!depublies}`.
 * Ainsi, notamment la prévisualisation continue de fonctionner.
 * 
 * Dans le fonctionnement normal de SPIP, dès que le critère `{statut}` 
 * est utilisé, le mécanisme automatique de sélection des éléments 
 * en fonction du statut et de l'éventuelle prévisualisation est désactivé.
 * 
 * Du coup, indiquer `{statut = publie}` pour ne plus sélectionner 
 * les éléments dépubliés (si l'option de les afficher par défaut est active) 
 * empêcherait la prévisualisation de fonctionner sur une telle boucle,
 * vu que le statut 'prop' (proposé à publication) en est exclu.
 * 
 * De même, indiquer dans une boucle `{statut IN publie,depublie}` 
 * (si l'option d'afficher les dépubliés est désactivée)
 * implique aussi que la prévisualisation d'un article `prop` 
 * ne serait pas vu dans cette boucle (puisque exclu également par la sélection).
 * 
 * Avec ce critère donc seul l'usage de `<BOUCLE_art(ARTICLES){depublies}>` 
 * bloque la prévisualisation d'un tel article en statut `prop`, 
 * car on demande forcément à voir les éléments en statut `depublie`.
 * Pour ce cas là (et seulement), on indique du coup à SPIP de désactiver 
 * le mécanisme automatique des statuts, ce qui simplifie un peu la requête SQL
 * générée, sans altérer le résultat final.
 */
function critere_depublies_dist($idb, &$boucles, $crit){
	$boucle = &$boucles[$idb];
	$boucle->modificateur['criteres']['depublie'] = true;

	// pour l'instant juste sur les articles.
	if ($boucle->type_requete == 'articles') {
		$id_table = $boucle->id_table;
		$mstatut = $id_table . '.statut';

		if ($crit->not) {
			$boucle->where[] = array("'!='", "'$mstatut'", "sql_quote('depublie')");
		} else {
			$boucle->where[] = array("'='", "'$mstatut'", "sql_quote('depublie')");
			# le statut est forcé, autant désactiver le calcul automatique, 
			# ça simplifie la requête SQL légèrement.
			$boucle->modificateur['criteres']['statut'] = true;
		}
	}
}
