<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	"aucune_date_definie" => "Aucune",
	"aucune_page" => "Aucune page dépubliée",
	"articles_depublies" => "Articles dépubliés",

	// C
	'cfg_visibilite_previsu' => 'Le statut « dépublié » est considéré comme previsualisable ?',
	'cfg_visibilite_previsu_explication' => 'Activer cette option permet de prévisualiser les éléments en statut « dépublié ».',
	'cfg_visibilite_publie' => 'Le statut « dépublié » est considéré comme le statut « publie » dans les boucles ?',
	'cfg_visibilite_publie_explication' => 'Par défaut les éléments en statut « dépublié » ne sont pas considérés comme visibles dans les boucles.
		Activer cette option permet de considérer le statut « dépublié » comme étant également à afficher, comme l\'est le statut « publie ».',
	'configurer_depublication' => 'Configurer les dépublications',

	// D
	"depublication_automatique_effectuee" => "Cet article a été dépublié
		automatiquement à la date qui avait été indiquée.",

	// E
	"erreur_statut_incorrect" => "Statut incorrect !",

	// L
	'label_date_depublication' => "Date de dépublication :",
	'label_statut_depublication' => "Statut après dépublication :",

	// P
	'pages_depubliees' => "Toutes les pages dépubliées",

	// S
	'statut_objet_depublie_titre' => "Objet dépublie",
	'statut_objet_depublie_texte' => "Dépublié",
);

