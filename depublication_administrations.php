<?php

/**
 * Plugin Dépublication
 *
 * Gestion de l'installation et de la désinstallation
 * 
 * @author Matthieu Marcillaud
 * @license GNU/GPL
 * @copyright 2010-2014
 * @package SPIP\Plugin\Depublication
 */

/**
 * Installation
 *
 * Création des champs supplémentaires sur spip_articles.
 * 
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */
function depublication_upgrade($nom_meta_base_version, $version_cible) {
	include_spip('base/create');

	$maj = array();
	$maj['create'] = array(array('maj_tables', array('spip_articles')));
	$maj['1.0.1']  = array(array('sql_alter', "TABLE spip_articles CHANGE COLUMN date_depublication date_depublication datetime DEFAULT '0000-00-00 00:00:00' NOT NULL"));
	$maj['1.0.2']  = array(array('sql_updateq', 'spip_articles', array('date_depublication' => '0000-00-00 00:00:00'), 'date_depublication=' . sql_quote('00-00-00 00:00:00')));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Désinstallation
 *
 * Suppression des champs ajoutés dans spip_articles
 *
 * @param string $nom_meta_base_version
 */
function depublication_vider_tables($nom_meta_base_version) {
	include_spip('inc/meta');
	include_spip('base/abstract_sql');
	sql_alter("TABLE spip_articles DROP COLUMN date_depublication");
	sql_alter("TABLE spip_articles DROP COLUMN statut_depublication");
	effacer_meta('depublication');
	effacer_meta($nom_meta_base_version);
}
