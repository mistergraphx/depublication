<?php
/**
 * Plugin Dépublication
 *
 * Gestion de la tâche cron dépubliant des articles à une date indiquée
 *
 * @author Matthieu Marcillaud
 * @license GNU/GPL
 * @copyright 2010-2014
 * @package SPIP\Plugin\Depublication
 */


/**
 * Cron pour dépublier les articles à dépublier
 *
 * @todo Gérer tout objet éditorial, pas uniquement les articles
 *
 * @param int $t
 * @return int
**/
function genie_depublier_objets_dist($t){
	
	include_spip('inc/config');
	$objets_depubliables = lire_config('depublication/depublie_objets');
	foreach($objets_depubliables as $objet){
		if($objet != ''){
			return genie_depublier_objet($t, $objet);
		}
	}
}

/**
 * Dépublier un objet si nécessaire
 *
 * @param int $t
 * @param string $objet
 */
function genie_depublier_objet($t, $objet) {

	$table = table_objet_sql($objet);
	$_id_table = id_table_objet($table);

	$work = sql_allfetsel(
		array(
			$_id_table,
			'date_depublication',
			'statut_depublication',
		),
		$table,
		array(
			'date_depublication < NOW()',
			'date_depublication > 0',
			'statut_depublication != '. sql_quote(''),
			'statut = '. sql_quote('publie'),
		)
	);


	if ($work) {

		include_spip('action/editer_objet');
		include_spip('depublication_pipelines'); // DEPUBLICATION_STATUT_PAR_DEFAUT

		foreach ($work as $w) {
			// attention aux statuts. Dans certains cas, on pourrait se retrouver avec un 'done'
			// (cas de republication d'un article qui fut dépublié par ce plugin, sans que personne n'ait retouché la date de dépublication.
			$statut = $w['statut_depublication'] ? $w['statut_depublication'] : DEPUBLICATION_STATUT_PAR_DEFAUT;
			if (!in_array($statut, array_keys(depublication_get_statuts_valides($objet)))) {
				$statut = DEPUBLICATION_STATUT_PAR_DEFAUT;
			}

			autoriser_exception('modifier', $objet, $w[$_id_table]);
			objet_instituer($objet, $w[$_id_table], array(
				'statut' => $statut,
			));
			// RAZ de la dépublication...
			objet_modifier($objet, $w[$_id_table], array('statut_depublication' => 'done'));
			autoriser_exception('modifier', $objet, $w[$_id_table], false);

			spip_log(
				'Depublication de ' . $objet . ':' . $w[$_id_table] . ' par le CRON à la date prévue du ' . $w['date_depublication'],
				"depublication." . _LOG_INFO_IMPORTANTE
			);
		}
	}

	return 1;
}