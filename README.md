LISEZ-MOI
=========

Qu'est-ce que le plugin de dépublication pour SPIP
--------------------------------------------------

Ce plugin permet d'indiquer optionnellement, pour les articles de SPIP,
une date de dépublication. Passé cette date, le statut de l'article
sera automatiquement modifié (par une tâche cron) en appliquant
un statut défini.

Dépendances
-----------

Dépublication nécessite SPIP >= 3.0


Installation
------------

Comme tout plugin pour SPIP : mettre ce dossier dans le répertoire `plugins`
de SPIP.


Détails
-------

Le plugin ajoute deux champs `date_depublication` et `statut_depublication`
sur les objets éditoriaux qui peuvent recevoir une date de dépublication
(actuellement seulement les articles).

Cette date et ce statut sont renseignés dans le formulaire `dater` de SPIP,
qui est étendu proprement (en utilisant les pipelines des formulaires CVT).

De la sorte, ces saisies s'intègrent parfaitement à l'espace privé et
la dépublication peut s'appliquer quelque soit l'objet éditorial.

Un nouveau statut `depublie` est ajouté, et est appliqué par défaut
comme statut lors de la dépublication. L'interface permet de choisir
un autre statut en même temps que l'on indique la date.

La constante `DEPUBLICATION_STATUT_PAR_DEFAUT` peut être définie pour
indiquer un statut différent par défaut (autre que `publie` !)


Configuration
-------------

La configuration permet d'indiquer la visibilité des éléments dépubliés
(ceux ayant obtenu le statut `depublie` plus précisément).

Par défaut avec SPIP ces éléments sont cachés des boucles standards, 
puisqu'elles  affichent uniquement les statuts `publie`. 
Pour afficher alors les éléments dépubliés sur l'espace public, il faut forcer 
le statut avec `{statut = depublie}` en critère de boucle.

Il est possible avec cette configuration de visibilité d'indiquer l'inverse : 
que le statut `depublie` soit considéré comme étant également à afficher 
par défaut ; au même titre que le statut `publie` donc. 


Critères dans les boucles
-------------------------

Si on souhaite afficher ou masquer à certains endroits dans les squelettes 
ces éléments dépubliés, et en fonction de la configuration choisie
(les éléments dépubliés sont affichés ou masqués par défaut dans les boucles), 
il faut l'indiquer explicitement sur la boucle.

On peut le réaliser avec le critère `{statut}` mais son utilisation 
désactive le calcul automatique par SPIP des statuts autorisés à s'afficher
sur la boucle, statuts, par ailleurs, qui changent si l'on est en mode prévisualisation.

- Écrire `{statut != depublie}` enlève les dépubliés, mais la présence du 
critère `statut`, qui annule le calcul automatique de SPIP donc, afficherait aussi 
les éléments proposés, en rédaction, s'il n'y a pas d'autre précision. 
C'est rarement ce que l'on souhaite.

- Écrire `{statut = publie}` affiche uniquement les publiés, mais dans ce cas là 
la prévisualisation d'autres statuts ne fonctionnerait pas sur ces boucles, 
ce qui est souvent gênant, car c'est ce qui est souhaité en prévisualisation : 
que les boucles publiées ajoutent les éléments non encore publiés.

- Écrire `{statut IN publie, depublie}` a le même souci des éléments prévisualisés, 
puisque les autres statuts ne seraient pas visibles dans ces boucles.

Ce plugin fournit le critère `{depublies}` et `{!depublies}` qui peut 
être pratique lorsque le mode de fonctionnement visible par défaut est configuré, 
car l'exclusion des dépubliés (avec `{!depublies}`) n'empêche pas ici le
fonctionnement de la prévisualisation des autres statuts.


Évolutions
----------

Quelques possibilités :

- pouvoir configurer les objets éditoriaux recevant une date de dépublication
- rendre possible l'application de dépublication à tous les objets éditoriaux
  (actuellement seuls les articles en reçoivent, mais le code est presque déjà
  entièrement prêt)



